
# PaymentwallBrick

## Installation

[PaymentwallBrick]can be installed
by adding `paymentwallbrick` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:paymentwall_brick, "~> 0.1.0"}
  ]
end
```

## Configuration

```elixir
config :paymentwallbrick,
  base_url: "https://api.paymentwall.com/api/brick/", # Either the production or test Paymentwall Brick API url
  public_key: "t_634792w6db1d4b223a97674d2e4c22", # Either the production or test Paymentwall Brick public key
  private_key: "t_7cf5ecrr55bsaa6fb5b5f53b7f33f1" # Either the production or test Paymentwall Brick private key
```

## Usage

### Charge

```elixir
charge_payload = %{
            "token" => "ot_4fbae80418dba74d551c21efa96cf790",
            "amount" => "9.99",
            "currency" => "USD",
            "email" => "test@test.com",
            "description" => "test"
          }

PaymentwallBrick.charge(charge_payload)
```

Example response:
```elixir
{
	201,	# Status code
	%{		# Response body 
		"amount" => "9.99",
		"amount_paid" => "9.99",
		"captured" => true,
		"card" => %{
			"country" => "US",
			"exp_month" => "01",
			"exp_year" => "2021",
			"last4" => "0002",
			"name" => "TEST PAYER",
			"token" => "t_78f78451169aea361c6893eb4a5c46",
			"type" => "VISA"
		},
		"created" => 1564333392,
		"currency" => "USD",
		"currency_paid" => "USD",
		"id" => "10751564333391_test",
		"object" => "charge",
		"refunded" => false,
		"risk" => "approved",
		"secure" => false,
		"support_link" => "http://example.com",
		"test" => 1
	}
}
```

### Define a pingback endpoint

```elixir
defmodule MyApp.PaymentController do

	# Payload example
	#	%{
	#		"goodsid" => "2",
	#		"is_test" => "1",
	#		"ref" => "12345",
	#		"sig" => "2597686e53ee27cd039bd2cd6694cc59",
	#		"sign_version" => "2",
	#		"slength" => "3",
	#		"speriod" => "4",
	#		"type" => "0",
	#		"uid" => "1"
	#	}

  def pingback(conn, payload) do
  	# PaymentwallPingback.is_valid? checks if the IP address
  	# is whitelisted and the payload signature is valid
    if  PaymentwallPingback.is_valid?(conn, payload) do
      case payload["type"] do
        "200" -> IO.puts("Processing")
        "201" -> IO.puts("Accepted")
        "202" -> IO.puts("Declined")
        "203" -> IO.puts("Voided")
        _ -> IO.puts("Not a PaymentwallBrick type")
      end      
      text(conn, "OK")
    else 
      text(conn, "ERROR")
    end
  end
end
```