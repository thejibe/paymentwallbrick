defmodule PaymentwallPingback do

  def is_valid?(conn, payload) do
    # @TODO Include IP check is_ip_address_valid?(conn.remote_ip)
    # Possible solution to grab the correct IP https://github.com/ajvondrak/remote_ip
    is_signature_valid?(payload)
  end

  def is_signature_valid?(%{"sig" => sig} = payload) do
    with  sig <- sig |> String.upcase,
          payload <- payload |> Map.delete("sig"),
          data <- payload |> Enum.map(fn {k,v} -> "#{k}=#{v}" end) |> Enum.join(""),
          data <- data<>secret(),
          md5 <- :crypto.hash(:md5, data) |> Base.encode16(),
          true <- (md5 == sig) do
      true
    else
      _err -> false
    end
  end

  def is_signature_valid?(_), do: false


  def is_ip_address_valid?(ip) do
    ip_whitelist = [
      {174,36,92,186},
      {174,36,96,66},
      {174,36,92,187},
      {174,36,92,192},
      {174,37,14,28}
    ]
    if ip_whitelist |> Enum.member?(ip) do
      true
    else
      cidr = InetCidr.parse("216.127.71.0/24")
      InetCidr.contains?(cidr, ip)
    end
  end

  defp secret do
    Application.get_env(:paymentwallbrick, :private_key, "")
  end

end