defmodule PaymentwallBrick do

  require Logger

  def default_headers do
    [
      {"X-ApiKey", Application.get_env(:paymentwallbrick, :private_key, "")},
      {"Content-Type", "application/x-www-form-urlencoded"}
    ]
  end

  def default_options do
    [
      {:timeout, 60_000},
      {:recv_timeout, 60_000}
    ]
  end

  def debug_mode? do
    Application.get_env(:paymentwallbrick, :debug, false)
  end

  def full_url(uri) do
    Application.get_env(:paymentwallbrick, :base_url, "")<>uri
  end

  def get(uri) do
    with full_url <- full_url(uri) do
      if debug_mode?(),
        do: Logger.debug("PaymentwallBrick Request - GET #{full_url}")
      res = HTTPoison.get(full_url, default_headers(), default_options())
      if debug_mode?(),
        do: Logger.debug("PaymentwallBrick Response - GET #{full_url}\n#{inspect res}")
      res |> response()
    end
  end

  def post(uri, payload \\ %{}) do
    with full_url <- full_url(uri) do
      if debug_mode?(),
        do: Logger.debug("PaymentwallBrick Request - POST #{full_url}\nPayload: #{inspect payload}")
      res = HTTPoison.post(full_url(uri), {:form, payload |> Map.to_list}, default_headers(), default_options())
      if debug_mode?(),
        do: Logger.debug("PaymentwallBrick Response - POST #{full_url}\n#{inspect res}")
      res |> response()
    end
  end

  def response({:ok, %HTTPoison.Response{body: "", status_code: code}}), do: {code, ""}

  def response({:ok, %HTTPoison.Response{body: raw, status_code: code}}), do: {code, Poison.decode!(raw)}

  def response({:error, %HTTPoison.Error{reason: :timeout}}), do: {:error, "Timeout"}

  def response({:error, %HTTPoison.Error{reason: ""}}), do: {:error, ""}

  def response({:error, %HTTPoison.Error{reason: reason}}), do: {:error, Poison.decode!(reason)}

  @doc """
    payload = %{
                "card[number]" => "4000000000000002",
                "card[exp_month]" => "01",
                "card[exp_year]" =>"2021",
                "card[cvv]" =>"123"
              }
  """
  def one_time_token(payload) do
    post("token", payload |> Map.merge(%{"public_key" => Application.get_env(:paymentwallbrick, :public_key, "")}))
  end

  @doc """
    payload = %{
                "token" => "ot_4fbae80418dba74d551c21efa96cf790",
                "amount" => "9.99",
                "currency" => "USD",
                "email" => "test@test.com",
                "description" => "test"
              }

  """
  def charge(payload) do
    post("charge", payload)
  end

  def get_charge_details(id) do
    get("charge/#{id}")
  end

  def refund_charge(id) do
    post("charge/#{id}/refund")
  end

  def void_charge(id) do
    post("charge/#{id}/void")
  end

  def capture_charge(id) do
    post("charge/#{id}/capture")
  end
end